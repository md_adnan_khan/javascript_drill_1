const {findCarWithId} = require('../problems/problem1.js');
const {inventory} = require('../carsData/carInventory.js');

let car = findCarWithId(inventory);

if(!(car === undefined)) {
    console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);
}