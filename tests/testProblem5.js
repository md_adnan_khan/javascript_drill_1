const {getCarManufacturedBeforeYear2000} = require('../problems/problem5.js');
const {inventory} = require('../carsData/carInventory.js');
const {getCarYearList} = require('../problems/problem4');

let carListOlderThenYear2000 = getCarManufacturedBeforeYear2000(inventory, getCarYearList(inventory));

if(!(carListOlderThenYear2000 === undefined)) {
    console.log(carListOlderThenYear2000);
}