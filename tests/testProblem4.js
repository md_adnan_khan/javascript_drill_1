const {inventory} = require('../carsData/carInventory');
const {getCarYearList} = require('../problems/problem4');

let carYearList = getCarYearList(inventory);

if(!(carYearList === undefined)) {
    console.log(carYearList);
}