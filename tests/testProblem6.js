const {getAudiAndBmwCar} = require('../problems/problem6');
const {inventory} = require('../carsData/carInventory');

let audiAndBmwCarsList = getAudiAndBmwCar(inventory);

if(!(audiAndBmwCarsList === undefined)) {
    let jsonString = JSON.stringify(audiAndBmwCarsList);
    console.log(jsonString);
}
