const {getSortedCarModelList} = require('../problems/problem3.js');
const {inventory} = require('../carsData/carInventory.js');

let sortedCarModelList = getSortedCarModelList(inventory);

if(!(sortedCarModelList === undefined)) {
    console.log(sortedCarModelList);
}