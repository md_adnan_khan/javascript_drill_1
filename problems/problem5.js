/* ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the 
previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length. */

function getCarManufacturedBeforeYear2000(inventory, carYearList) {
    if(inventory === null || inventory === undefined ||  inventory.length === 0 || !Array.isArray(inventory)) { //Handle error and edge cases
        console.log(`Input is not a valid array, Enter a valid input`);
        return;
    }

    let totalCarsMadeBefore2000 = 0;
    for(let year of carYearList) { //To find total cars manufactured before year 2000 according to question
        if(year < 2000) {
            totalCarsMadeBefore2000++;
        }
    }

    let carListMadeBeforeYear2000 = [];
    for(let car of inventory) {
        if(car.car_year < 2000) {
            carListMadeBeforeYear2000.push(car); //push all the cars manufactured before year: 2000
        }
    }
    
    console.log(`Total cars manufactured before year '2000': ${totalCarsMadeBefore2000}`);
    return carListMadeBeforeYear2000;
}

module.exports.getCarManufacturedBeforeYear2000 = getCarManufacturedBeforeYear2000;

