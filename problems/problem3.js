function getSortedCarModelList(inventory) { 
    if(inventory === null || inventory === undefined ||  inventory.length === 0 || !Array.isArray(inventory)) { //Handle error and edge cases
        console.log(`Input is not a valid array, Enter a valid input`);
        return;
    }

    let sortedCarModelList = [];
    for(let car of inventory) {
        sortedCarModelList.push(car.car_model.toUpperCase()); //Convert to 'upper' case and sort bcs 'riolet' is in smallCase
    }

    return sortedCarModelList.sort();
}

module.exports.getSortedCarModelList = getSortedCarModelList;