function findCarWithId(carInventory) {
    if(carInventory === null || carInventory === undefined || carInventory.length === 0 || !Array.isArray(carInventory)) { //Handle error and edge cases
        console.log(`Input is not a valid array, Enter a valid input`);
        return;
    }

    for(let car of carInventory) {
        if(car.id === 33) {
            return car;
        }
    }
}

module.exports.findCarWithId = findCarWithId;
