function getLastCar(inventory) {
    if(inventory === null || inventory === undefined ||  inventory.length === 0 || !Array.isArray(inventory)) { //Handle error and edge cases
        console.log(`Input is not a valid array, Enter a valid input`);
        return;
    } 

    let lastCarIndex = inventory.length - 1;
    let lastCar = inventory[lastCarIndex];
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`); // console last car
}

module.exports.getLastCar = getLastCar;