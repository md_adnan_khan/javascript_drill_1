function getCarYearList(inventory) {
    if(inventory === null || inventory === undefined ||  inventory.length === 0 || !Array.isArray(inventory)) { //Handle error and edge cases
        console.log(`Input is not a valid array, Enter a valid input`);
        return;
    }

    let carYearList = [];
    for(let car of inventory) {
        carYearList.push(car.car_year);
    }
    
    return carYearList;
}

module.exports.getCarYearList = getCarYearList;