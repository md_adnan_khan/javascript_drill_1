/* ==== Problem #6 ====
A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains 
BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console. */

function getAudiAndBmwCar(inventory) {
    if(inventory === null || inventory === undefined ||  inventory.length === 0 || !Array.isArray(inventory)) { //Handle error and edge cases
        console.log(`Input is not a valid array, Enter a valid input`);
        return;
    }

    let audiAndBmwCarList = [];
    for(let car of inventory) {
        if(car.car_make == 'Audi' || car.car_make == 'BMW') {
            audiAndBmwCarList.push(car);
        }
    }

    return audiAndBmwCarList;
}

module.exports.getAudiAndBmwCar = getAudiAndBmwCar;